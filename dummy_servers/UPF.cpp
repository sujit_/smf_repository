/************** DUMMY UPF SERVER ****************/
/*            Author: ANISH DUTTA               */
/*      Last Updated: 20/05/2018                */
/************************************************/
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <bits/stdc++.h>

#include "PFCP/AssociationSetupRequest.hpp"
#include "PFCP/AssociationSetupResponse.hpp"
#include "PFCP/AssociationReleaseRequest.hpp"
#include "PFCP/AssociationReleaseResponse.hpp"
#include "PFCP/HeartbeatRequest.hpp"
#include "PFCP/HeartbeatResponse.hpp"
#include "PFCP/PFCPMessage.hpp"
#include "PFCP/SessionEstablishmentRequest.hpp"
#include "PFCP/SessionEstablishmentResponse.hpp"
#include "PFCP/SessionModificationRequest.hpp"
#include "PFCP/SessionModificationResponse.hpp"
#include "PFCP/SessionDeletionRequest.hpp"
#include "PFCP/SessionDeletionResponse.hpp"
#include "MsgInfo.hpp"
using namespace std;

uint32_t MAX_MSG_LEN = 1500;

bool ResolveHostName(char* hostname , char* ip) {
  struct hostent *he;
  struct in_addr **addr_list;
  int i;
  if ( (he = gethostbyname( hostname ) ) == NULL) {
    herror("gethostbyname");
    return false;
  }
  addr_list = (struct in_addr **) he->h_addr_list;
  for(i = 0; addr_list[i] != NULL; i++) {
    strcpy(ip , inet_ntoa(*addr_list[i]) );
    return true;
  }
  return false;
}

shared_ptr <PFCPMessage> AssociationSetupRequestHandler(shared_ptr <PFCPMessage> message) {
  auto assoc_setup_req = static_pointer_cast <AssociationSetupRequest> (message);
  auto assoc_setup_rsp = static_pointer_cast <AssociationSetupResponse> (AssociationSetupResponse::CreateMessage(MAX_MSG_LEN));
  assoc_setup_rsp->msg_header_.node_msg_header = assoc_setup_req->msg_header_.node_msg_header;
  assoc_setup_rsp->msg_header_.node_msg_header.type = pfcp::ASSOCIATION_SETUP_RESPONSE;
  assoc_setup_rsp->node_id_m_.flags = 1;
  assoc_setup_rsp->cause_m_.flags = 1;
  assoc_setup_rsp->cause_m_.cause_value = pfcp::REQUEST_ACCEPTED;
  assoc_setup_rsp->recovery_time_stamp_m_.flags = 1;
  assoc_setup_rsp->Encode();
  return static_pointer_cast <PFCPMessage> (assoc_setup_rsp);
}

shared_ptr <PFCPMessage> HeartbeatRequestHandler(shared_ptr <PFCPMessage> message) {
  auto heartbeat_req = static_pointer_cast <HeartbeatRequest> (message);
  auto heartbeat_rsp = static_pointer_cast <HeartbeatResponse> (HeartbeatResponse::CreateMessage(MAX_MSG_LEN));
  heartbeat_rsp->msg_header_.node_msg_header = heartbeat_req->msg_header_.node_msg_header;
  heartbeat_rsp->msg_header_.node_msg_header.type = pfcp::HEARTBEAT_RESPONSE;
  heartbeat_rsp->recovery_time_stamp_m_.flags = 1;
  heartbeat_rsp->Encode();
  return static_pointer_cast <PFCPMessage> (heartbeat_rsp);
}

shared_ptr <PFCPMessage> AssociationReleaseRequestHandler(shared_ptr <PFCPMessage> message) {
  auto assoc_release_req = static_pointer_cast <AssociationReleaseRequest> (message);
  auto assoc_release_rsp = static_pointer_cast <AssociationReleaseResponse> (AssociationReleaseResponse::CreateMessage(MAX_MSG_LEN));
  assoc_release_rsp->msg_header_.node_msg_header = assoc_release_req->msg_header_.node_msg_header;
  assoc_release_rsp->msg_header_.node_msg_header.type = pfcp::ASSOCIATION_RELEASE_RESPONSE;
  assoc_release_rsp->node_id_m_.flags = 1;
  assoc_release_rsp->cause_m_.flags = 1;
  assoc_release_rsp->cause_m_.cause_value = pfcp::REQUEST_ACCEPTED;
  assoc_release_rsp->Encode();
  return static_pointer_cast <PFCPMessage> (assoc_release_rsp);
}

shared_ptr <PFCPMessage> SessionEstablishmentRequestHandler(shared_ptr <PFCPMessage> message) {
  auto session_est_req = static_pointer_cast <SessionEstablishmentRequest> (message);
  auto session_est_rsp = static_pointer_cast <SessionEstablishmentResponse> (SessionEstablishmentResponse::CreateMessage(MAX_MSG_LEN));
  session_est_rsp->msg_header_.session_msg_header = session_est_req->msg_header_.session_msg_header;
  session_est_rsp->msg_header_.session_msg_header.type = pfcp::SESSION_ESTABLISHMENT_RESPONSE;
  session_est_rsp->node_id_m_.flags = 1;
  session_est_rsp->cause_m_.flags = 1;
  session_est_rsp->cause_m_.cause_value = pfcp::REQUEST_ACCEPTED;
  pfcp::IEFTEID f_teid{};
  f_teid.flags = 1;
  f_teid.teid = 1;
  f_teid.v4 = 1;
  f_teid.ipv4_addr.type = AF_INET;
  f_teid.ipv4_addr.ip[0] = 1;
  f_teid.ipv4_addr.ip[1] = 1;
  f_teid.ipv4_addr.ip[2] = 1;
  f_teid.ipv4_addr.ip[3] = 1;
  f_teid.ipv4_addr.ip[4] = 0;
  for (auto& create_pdr : session_est_req->create_pdr_m_) {
    if (1 == create_pdr.flags) {
      pfcp::IECreatedPDR created_pdr;
      created_pdr.flags = 1;
      created_pdr.pdr_id_m = create_pdr.pdr_id_m;
      created_pdr.local_f_teid_c = f_teid;
      session_est_rsp->created_pdr_c_.push_back(created_pdr);
    }
  }
  session_est_rsp->Encode();
  return static_pointer_cast <PFCPMessage> (session_est_rsp);
}

shared_ptr <PFCPMessage> SessionModificationRequestHandler(shared_ptr <PFCPMessage> message) {
  auto session_mod_req = static_pointer_cast <SessionModificationRequest> (message);
  auto session_mod_rsp = static_pointer_cast <SessionModificationResponse> (SessionModificationResponse::CreateMessage(MAX_MSG_LEN));
  session_mod_rsp->msg_header_.session_msg_header = session_mod_req->msg_header_.session_msg_header;
  session_mod_rsp->msg_header_.session_msg_header.type = pfcp::SESSION_MODIFICATION_RESPONSE;
  session_mod_rsp->cause_m_.flags = 1;
  session_mod_rsp->cause_m_.cause_value = pfcp::REQUEST_ACCEPTED;
  for (auto& create_pdr : session_mod_req->create_pdr_c_) {
    if (1 == create_pdr.flags) {
      pfcp::IECreatedPDR created_pdr;
      created_pdr.flags = 1;
      created_pdr.pdr_id_m = create_pdr.pdr_id_m;
      session_mod_rsp->created_pdr_c_.push_back(created_pdr);
    }
  }
  session_mod_rsp->Encode();
  return static_pointer_cast <PFCPMessage> (session_mod_rsp);
}

shared_ptr <PFCPMessage> SessionDeletionRequestHandler(shared_ptr <PFCPMessage> message) {
  auto session_del_req = static_pointer_cast <SessionDeletionRequest> (message);
  auto session_del_rsp = static_pointer_cast <SessionDeletionResponse> (SessionDeletionResponse::CreateMessage(MAX_MSG_LEN));
  session_del_rsp->msg_header_.session_msg_header = session_del_req->msg_header_.session_msg_header;
  session_del_rsp->msg_header_.session_msg_header.type = pfcp::SESSION_DELETION_RESPONSE;
  session_del_rsp->cause_m_.flags = 1;
  session_del_rsp->cause_m_.cause_value = pfcp::REQUEST_ACCEPTED;
  session_del_rsp->Encode();
  return static_pointer_cast <PFCPMessage> (session_del_rsp);
}

shared_ptr <PFCPMessage> CreateReponse(shared_ptr <PFCPMessage> message) {
  shared_ptr <PFCPMessage> res_msg;
  message->Decode();
  switch(message->GetMsgType()) {
    case pfcp::ASSOCIATION_SETUP_REQUEST: {
      res_msg = AssociationSetupRequestHandler(message);
      break;
    }
    case pfcp::HEARTBEAT_REQUEST: {
      res_msg = HeartbeatRequestHandler(message);
      break;
    }
    case pfcp::ASSOCIATION_RELEASE_REQUEST: {
      res_msg = AssociationReleaseRequestHandler(message);
      break;
    }
    case pfcp::SESSION_ESTABLISHMENT_REQUEST: {
      res_msg = SessionEstablishmentRequestHandler(message);
      break;
    }
    case pfcp::SESSION_MODIFICATION_REQUEST: {
      res_msg = SessionModificationRequestHandler(message);
      break;
    }
    case pfcp::SESSION_DELETION_REQUEST: {
      res_msg = SessionDeletionRequestHandler(message);
      break;
    }
  }
  return res_msg;
}

int main(int argc, char* argv[]){
  int udpSocket, nBytes;
  char buffer[1024];
  sockaddr_in serverAddr;
  sockaddr_storage serverStorage;
  socklen_t addr_size;
  int i;
  MsgInfo msgInfo;
  msgInfo.Initialize();

  char hostname[] = "upf.polarisnetworks.net";
  if (argc > 1) {
    strcpy(hostname, argv[1]);
  }
  char ip[100];
  ResolveHostName(hostname , ip);
  /*Create UDP socket*/
  udpSocket = socket(PF_INET, SOCK_DGRAM, 0);

  /*Configure settings in address struct*/
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(8805);
  serverAddr.sin_addr.s_addr = inet_addr(ip);
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

  /*Bind socket with address struct*/
  bind(udpSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

  /*Initialize size variable to be used later on*/
  addr_size = sizeof serverStorage;

  while(1){
    int n = recvfrom(udpSocket,buffer,1024,0,(struct sockaddr *)&serverStorage, &addr_size);
    auto rsp = CreateReponse(PFCPMessage::CreateMessage(n, (uint8_t*)buffer));
    sendto(udpSocket,rsp->GetBuffer()->GetBuf(),rsp->GetBuffer()->GetPos(),0,(struct sockaddr *)&serverStorage,addr_size);
  }
  msgInfo.Finalize();
  return 0;
}
