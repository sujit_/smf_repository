// Copyright 2018 Polaris Networks (www.polarisnetworks.net).

#ifndef FRAMEWORK_JSON_HPP_
#define FRAMEWORK_JSON_HPP_

#include "ThirdParty/include/rapidjson/document.h"
#include "ThirdParty/include/rapidjson/istreamwrapper.h"
#include "ThirdParty/include/rapidjson/ostreamwrapper.h"
#include "ThirdParty/include/rapidjson/prettywriter.h"
#include "ThirdParty/include/rapidjson/stringbuffer.h"
#include "ThirdParty/include/rapidjson/writer.h"

const int64_t MAX_ARRAY_SIZE = 100;
#define JSON_STRINGIFY(jsonobj, str, buf, w)\
    w->Reset(*buf);\
    (jsonobj)->Accept(*w);\
    str = (buf)->GetString();\
    (buf)->Clear();

#define JSON_ADD_STRING_MEMBER(k, v, jsonobj) {\
    rapidjson::Value key(k, (jsonobj)->GetAllocator());\
    rapidjson::Value val(v, (jsonobj)->GetAllocator());\
    (jsonobj)->AddMember(key, val, (jsonobj)->GetAllocator());\
}

#define JSON_ADD_MEMBER(k, v, jsonobj) {\
    rapidjson::Value key(k, (jsonobj)->GetAllocator());\
    rapidjson::Value val(v);\
    (jsonobj)->AddMember(key, val, (jsonobj)->GetAllocator());\
}

#endif  // FRAMEWORK_JSON_HPP_
