/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <nghttp2/asio_http2_server.h>
#include <iostream>
#include <unordered_map>
#include <string>
#include <vector>

#include "ThirdParty/include/rapidjson/document.h"
#include "ThirdParty/include/rapidjson/istreamwrapper.h"
#include "ThirdParty/include/rapidjson/ostreamwrapper.h"
#include "ThirdParty/include/rapidjson/prettywriter.h"
#include "ThirdParty/include/rapidjson/stringbuffer.h"
#include "ThirdParty/include/rapidjson/writer.h"
#include "MsgInfo.hpp"

using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::server;

std::vector<std::string> GetTokens(std::string line)
{
    std::string delim = "/";
    std::vector<std::string> results;
    size_t lastOffset = 0;

    while(true)
    {
        size_t offset = line.find_first_of(delim, lastOffset);
        results.push_back(line.substr(lastOffset, offset-lastOffset));
        if (offset == std::string::npos)
            break;
        else
            lastOffset = offset + 1; // add one to skip the delimiter
    }
    return results;

}

int main(int argc, char *argv[]) {
  boost::system::error_code ec;
  http2 server;
  server.num_threads(4);
  MsgInfo msgInfo;
  msgInfo.Initialize();


  server.handle("/ping", [&](const request &req, const response & rsp) {
    rsp.write_head(200);
    std::cout << "/ping received. UDM Alive" << std::endl;
    msgInfo.LOG("UDM", "/ping received. UDM Alive");
    rsp.end("UDM Alive");
  });

  server.handle("/nudm-uecm/v1/", [](const request &req, const response & rsp) {

    req.on_data([&](const uint8_t *data, std::size_t len) {
    std::string data1{""};
    if (len != 0) {
      std::string request_data(data, data + len);

      rapidjson::Document json_doc;
      json_doc.Parse(request_data.c_str());
      std:: string DNN = json_doc["dnn"].GetString();
      std:: string SMFID = json_doc["smfId"].GetString();
      uint8_t pduSessionId = json_doc["pduSessionId"].GetInt();
      rapidjson::Document j_res;
      j_res.SetObject();
      // rapidjson::Value supi(SUPI.c_str(), SUPI.length());
      rapidjson::Value psi(pduSessionId);
      rapidjson::Value dnn(DNN.c_str(),DNN.length());
      rapidjson::Value smfId(SMFID.c_str(),SMFID.length());
      // j_res.AddMember("supi", supi, j_res.GetAllocator());
      j_res.AddMember("pduSessionId", psi, j_res.GetAllocator());
      j_res.AddMember("dnn", dnn, j_res.GetAllocator());
      j_res.AddMember("smfId", smfId, j_res.GetAllocator());
      rapidjson::StringBuffer j_buffer_;
      rapidjson::Writer<rapidjson::StringBuffer> writer_(j_buffer_);
      j_res.Accept(writer_);
      std::string data1 = j_buffer_.GetString();
      // std::cout << "SUPI :: " << json_doc["supi"].GetString() << std::endl;
      std::cout << "PDU Sess ID :: " << json_doc["pduSessionId"].GetInt() << std::endl;
      std::cout << "DNN :: " << json_doc["dnn"].GetString() << std::endl;
      std::cout << "SMF ID :: " << json_doc["smfId"].GetString() << std::endl;
      rsp.write_head(201);
      rsp.end(data1);
    } else {
      rsp.write_head(204);
      rsp.end(data1);	
	}
  });
  });

  server.handle("/nudm-sdm/v1/", [](const request &req, const response & rsp) {
    nghttp2::asio_http2::uri_ref uri = req.uri();
    std::string path = uri.path;
    std::vector<std::string> segments = GetTokens(path);

    std::string SUPI = segments[3];

    rapidjson::Document j_res;
    j_res.SetObject();
    j_res.AddMember("singleNssai.sst", 22, j_res.GetAllocator());
    j_res.AddMember("singleNssai.sd", "3", j_res.GetAllocator());
    
    /* DnnConfiguration */
    /* DNN */
    j_res.AddMember("dnnConfiguration.dnn", "internet.TestNetwork",
            j_res.GetAllocator());
    
    /* PduSessionTypes */
    j_res.AddMember("dnnConfiguration.pduSessionTypes.defaultSessionType", "IPV4",
            j_res.GetAllocator());
    
    /* SscModes */
    j_res.AddMember("dnnConfiguration.sscModes.defaultSscMode", "SSC_MODE_1",
            j_res.GetAllocator());
    
    /* Static IP Address */
    j_res.AddMember("dnnConfiguration.staticIpAddress", "10.10.10.1",
            j_res.GetAllocator());
    
    /* Session AMBR */
    j_res.AddMember("dnnConfiguration.sessionAMBR.uplink", "1024", j_res.GetAllocator());
    j_res.AddMember("dnnConfiguration.sessionAMBR.downlink", "1024", j_res.GetAllocator());
    
    /* LadnIndicator */
    j_res.AddMember("dnnConfiguration.ladnIndicator", 1, j_res.GetAllocator());
    
    /* 5gQosProfile */
    j_res.AddMember("dnnConfiguration.qosProfile.qi", 6, j_res.GetAllocator());
    j_res.AddMember("dnnConfiguration.qosProfile.arp.priorityLevel", 2,
            j_res.GetAllocator());
    j_res.AddMember("dnnConfiguration.qosProfile.arp.preemptCap", "MAY_PREEMPT",
            j_res.GetAllocator());
    j_res.AddMember("dnnConfiguration.qosProfile.arp.preemptVuln", "PREEMPTABLE",
            j_res.GetAllocator());
    
    rapidjson::StringBuffer j_buffer_;
    rapidjson::Writer<rapidjson::StringBuffer> writer_(j_buffer_);
    j_res.Accept(writer_);
    std::string data = j_buffer_.GetString();
    rsp.write_head(200);
    rsp.end(data);
  });
 
  if (server.listen_and_serve(ec, "udm.polarisnetworks.net", "4040")) {
    std::cerr << "error: " << ec.message() << std::endl;
    msgInfo.Finalize();
  }
}
