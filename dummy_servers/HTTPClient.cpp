
#include <nghttp2/asio_http2_server.h>
#include "HTTPClient.h"

using boost::asio::ip::tcp;
using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::server;

void DoWork(boost::asio::io_service* ioService)
{
  ioService->run();
}

HTTPClient::HTTPClient()
{
  ioService_ = new boost::asio::io_service;
  work_ = new boost::asio::io_service::work(*ioService_);
  workers_.create_thread(boost::bind(&DoWork, ioService_));
}

void HTTPClient::Connect(const std::string remoteAddress, 
  const std::string remotePort, const std::string sessionId, Connect_Cb OnConnect)
{
  asio_http2::client::session* session = new asio_http2::client::session(
    *ioService_, remoteAddress, remotePort);
  
  sessionMap_[sessionId] = session;
    
  session->on_connect([&, sessionId, OnConnect](tcp::resolver::iterator endpoint_it)
  {
    OnConnect(sessionId, true);
  });

  session->on_error([&, sessionId, OnConnect](const boost::system::error_code& ec)
  {
    delete sessionMap_[sessionId];
    sessionMap_.erase(sessionId);
    OnConnect(sessionId, false);
  });
}

void HTTPClient::Get(const std::string sessionId, const std::string uri, 
    const std::string requestId, Response_Cb OnResponse)
{
  boost::system::error_code ec;
  if (!sessionMap_[sessionId])
  {
    return;
  }
  auto request = sessionMap_[sessionId]->submit(ec, "GET", uri);

  request->on_response([&, requestId, OnResponse](const asio_http2::client::response& response)
  {
    response.on_data([&, requestId, OnResponse](const uint8_t* buffer, std::size_t bufferLen)
    {
      // TODO: this may not be the whole response, this may be only a chunk.
      // HTTPClient may need to store all chunks before calling OnResponse().
      OnResponse(requestId, buffer, bufferLen);
    });
  });

  request->on_close([](uint32_t errorCode)
  {
    //Nothing to do?
  });
}

void HTTPClient::Relay(const std::string sessionId, const std::string uri, 
    const std::string requestId, const server::response* res, Relay_Response_Cb OnResponse)
{
  boost::system::error_code ec;
  if (!sessionMap_[sessionId])
  {
    return;
  }
  auto request = sessionMap_[sessionId]->submit(ec, "GET", uri);

  request->on_response([&, requestId, OnResponse](const asio_http2::client::response& response)
  {
    response.on_data([&, requestId, OnResponse](const uint8_t* buffer, std::size_t bufferLen)
    {
      // TODO: this may not be the whole response, this may be only a chunk.
      // HTTPClient may need to store all chunks before calling OnResponse().
      OnResponse(requestId, buffer, bufferLen, res);
    });
  });

  request->on_close([](uint32_t errorCode)
  {
    //Nothing to do?
  });
}
void HTTPClient::Post(const std::string sessionId, const std::string uri, 
    const std::string data, const std::string requestId,
        const nghttp2::asio_http2::header_map header, Response_Cb OnResponse)
{
  boost::system::error_code ec;
  if (!sessionMap_[sessionId])
  {
    return;
  }
  auto request = sessionMap_[sessionId]->submit(ec, "POST", uri, data, header);

  request->on_response([&, requestId, OnResponse](const asio_http2::client::response& response)
  {
    response.on_data([&, requestId, OnResponse](const uint8_t* buffer, std::size_t bufferLen)
    {
      // TODO: this may not be the whole response, this may be only a chunk.
      // HTTPClient may need to store all chunks before calling OnResponse().
      OnResponse(requestId, buffer, bufferLen);
    });
  });

  request->on_close([](uint32_t errorCode)
  {
    //Nothing to do?
  });
}

void HTTPClient::Disconnect(const std::string sessionId)
{
  if (!sessionMap_[sessionId])
  {
    return;
  }
  sessionMap_[sessionId]->shutdown();
  delete sessionMap_[sessionId];
  sessionMap_.erase(sessionId);
}

HTTPClient::~HTTPClient()
{
  // Sessions must be cleaned first via Disconnect()
  ioService_->stop();
  workers_.join_all();
  delete work_;
  delete ioService_;
}
