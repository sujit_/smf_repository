/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <nghttp2/asio_http2_server.h>
#include <iostream>
#include <unordered_map>
#include <string>
#include "Npcf_SMPolicyControl.hpp"
#include "ThirdParty/include/rapidjson/document.h"
#include "ThirdParty/include/rapidjson/istreamwrapper.h"
#include "ThirdParty/include/rapidjson/ostreamwrapper.h"
#include "ThirdParty/include/rapidjson/prettywriter.h"
#include "ThirdParty/include/rapidjson/stringbuffer.h"
#include "ThirdParty/include/rapidjson/writer.h"

using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::server;

int main(int argc, char *argv[]) {
  boost::system::error_code ec;
  http2 server;
  server.num_threads(4);
  
  server.handle("/ping", [](const request &req, const response & rsp) {
    rsp.write_head(200);
    std::cout << "/ping received. PCF Alive" << "" << std::endl;
    rsp.end("PCF Alive");
  });
  
  server.handle("/npcf-smpolicycontrol/v1/sm-policies", [](const request &req, const response & rsp) {
    std::cout << "/npcf-smpolicycontrol/v1/sm-policies received" << "" << std::endl;
    req.on_data([&](const uint8_t *data, std::size_t len) {
    if (len != 0) {
      std::string res_data(data, data + len);
      
      SmPolicyContextData PolicyContextData;
      rapidjson::Document j_res;
      j_res.SetObject();
      j_res.Parse(res_data.c_str());
      PolicyContextData.decode(j_res, &PolicyContextData);
      std:: string SUPI = PolicyContextData.supi;
      uint8_t pdu_session_id = PolicyContextData.pduSessionId;
     
      std::string data1{""};
      
      SmPolicyDecision PolicyDecision;
      
      SessionRule Session_Rule;
      Session_Rule.sessRuleId = 1;
      Session_Rule.authSessAmbrUl = "102400";
      Session_Rule.authSessAmbrDl = "102400";
      DefaultQosInformation DefaultQos_Information;
      DefaultQos_Information._5qi = 6;
      DefaultQos_Information.arp.priorityLevel = 1;
      DefaultQos_Information.arp.preemptCap = "TRUE";
      DefaultQos_Information.arp.preemptVuln = "FALSE";
      Session_Rule.authDefaultQos = DefaultQos_Information;
      PolicyDecision.sessRules[0] = Session_Rule;
      PolicyDecision.sessRules_len = 1;

      PccRule pcc_Rules;
      pcc_Rules.pccRuleId = "default";
      pcc_Rules.appId = "1";
      pcc_Rules.precedence = 1;
      PolicyDecision.pccRules[0] = pcc_Rules;
      PolicyDecision.pccRules_len = 1;

      ChargingData Chg_Decs;
      Chg_Decs.chgId = 1;
      Chg_Decs.meteringMethod = "volume";
      Chg_Decs.online = 1;
      Chg_Decs.offline = 0;
      Chg_Decs.ratingGroup = "1";
      Chg_Decs.serviceId = "1";
      Chg_Decs.sponsorId = "1";
      Chg_Decs.reportingLevel = "";
      Chg_Decs.afChargingIdentifier = "1";
      Chg_Decs.chargingInformation.primaryChfAddress = "10.10.10.10";
      PolicyDecision.ChgDecs[0] = Chg_Decs;
      PolicyDecision.ChgDecs_len = 1;


      rapidjson::Document j_request;
      j_request.SetObject();
      PolicyDecision.encode(PolicyDecision, &j_request, &j_request);
      rapidjson::StringBuffer j_buffer;
      rapidjson::Writer<rapidjson::StringBuffer> writer_(j_buffer);
      j_request.Accept(writer_);
      data1 = j_buffer.GetString();
      
      std::cout << "SUPI :: " << SUPI<< std::endl;
      std::cout << "PDU Sess ID :: " << static_cast<int32_t>(pdu_session_id) << std::endl;
      rsp.write_head(201);
      rsp.end(data1);
    }
  });
  });

  if (server.listen_and_serve(ec, "pcf.polarisnetworks.net", "4040")) {
    std::cerr << "error: " << ec.message() << std::endl;
  }
}
  
