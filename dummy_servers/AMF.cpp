/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <fstream>
#include <string>
#include <thread>  // NOLINT

#include <nghttp2/asio_http2_server.h>
#include <nghttp2/asio_http2_client.h>
#include <iostream>
#include "HTTPClient.h"
#include "Framework/Log.hpp"

#include "ThirdParty/include/rapidjson/document.h"
#include "ThirdParty/include/rapidjson/istreamwrapper.h"
#include "ThirdParty/include/rapidjson/ostreamwrapper.h"
#include "ThirdParty/include/rapidjson/prettywriter.h"
#include "ThirdParty/include/rapidjson/stringbuffer.h"
#include "ThirdParty/include/rapidjson/writer.h"
#include "MsgInfo.hpp"
#include "NGAP/ASN1Includes.hpp"
#include "NGAP/NGAPMessage.hpp"

using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::server;
using namespace nghttp2::asio_http2::client;
using boost::asio::ip::tcp;

bool s1 = false;
std::string sId1 = "SMF-Connection";
std::string rId1 = "nsmf-pdusession/v1/sm-contexts";

const server::response *gRes;

void onconnect(const std::string sessionId, bool status)
{
  if (status == false)
    std::cout << sessionId << " failed to establish" << std::endl;
  else
    std::cout << sessionId << " established" << std::endl;
  
  s1 = status;
}

void onresponse(const std::string requestId, const uint8_t* buf, 
  std::size_t buflen)
{
  if (buflen > 0)
    std::cout << "Response from " << requestId << " :: " << buf << std::endl;
  gRes->end("SMF Alive\n");
}

std::string BuildHTTPBodyPart(const std::string data,
                                     const std::string boundary,
                                     const std::string content_id,
                                     const std::string content_type) {
  std::string body_part = "";
  body_part.append("\r\n--" + boundary + "\r\n");
  body_part.append("Content-Type: " + content_type + "\r\n");
  body_part.append("Content-ID: " + content_id + "\r\n\r\n");
  body_part.append(data);
  return body_part;
}



std::string SmContextCreateData() {

  std::string instance = "1";
  std::string SUPI = "imsi-00" + instance + "0" + instance + "999999999" + "1";
  uint8_t pdu_session_id = 5;
  std::string DNN = "internet.TestNetwork";
  uint8_t SNSSAI_SST = 1;
  std::string SNSSAI_SD = "000002";
  std::string AMFID = "amf.TestNetwork";
  std::string ANTYPE = "3GPP_ACCESS";
  std::string AMFURI = "http://amf.polarisnetworks.net:4040/namf-pdusession/v1/sm-contexts";
  std::string CONTENT_ID = "amfnas@polarisnetworks.net";

  rapidjson::Document j_res;
  j_res.SetObject();
  rapidjson::Document snssai;
  snssai.SetObject();
  rapidjson::Value snssai_sst_value(SNSSAI_SST);
  rapidjson::Value snssai_sd_value(SNSSAI_SD.c_str(), SNSSAI_SD.length());
  snssai.AddMember("sst", snssai_sst_value, j_res.GetAllocator());
  snssai.AddMember("sd", snssai_sd_value, j_res.GetAllocator());
  rapidjson::Value supi(SUPI.c_str(), SUPI.length());
  rapidjson::Value psi(pdu_session_id);
  rapidjson::Value dnn(DNN.c_str(), DNN.length());
  rapidjson::Value amfid(AMFID.c_str(), AMFID.length());
  rapidjson::Value antype(ANTYPE.c_str(), ANTYPE.length());
  rapidjson::Value amfuri(AMFURI.c_str(), AMFURI.length());
  rapidjson::Value contentId(CONTENT_ID.c_str(), CONTENT_ID.length());
  rapidjson::Document n1SmMsg;
  n1SmMsg.SetObject();
  n1SmMsg.AddMember("contentId", contentId, j_res.GetAllocator());

  j_res.AddMember("supi", supi, j_res.GetAllocator());
  j_res.AddMember("pdu_session_id", psi, j_res.GetAllocator());
  j_res.AddMember("dnn", dnn, j_res.GetAllocator());
  j_res.AddMember("amfId", amfid, j_res.GetAllocator());
  j_res.AddMember("anType", antype, j_res.GetAllocator());
  j_res.AddMember("smContextStatusUri", amfuri, j_res.GetAllocator());
  j_res.AddMember("n1SmMsg", n1SmMsg, j_res.GetAllocator());
  j_res.AddMember("snssai", snssai, j_res.GetAllocator());

  rapidjson::StringBuffer j_buffer_;
  rapidjson::Writer<rapidjson::StringBuffer> writer_(j_buffer_);
  j_res.Accept(writer_);
  std::string json_data = j_buffer_.GetString();
  
  std::string http_boundary = "polarishttpmultipartboundary";
  std::string data = "";
  data.append(BuildHTTPBodyPart(json_data,
                                http_boundary,
                                "json0001@polarisnetworks.net",
                                "application/json"));
  
  std::size_t nas_buffer_len = 7;
  uint8_t nas_buffer[] = {0x2E, 0x13, 0x01, 0xC1, 0x91, 0xA1};
  
  data.append(BuildHTTPBodyPart(std::string(reinterpret_cast<char*>(nas_buffer),
                                              nas_buffer_len),
                                http_boundary,
                                "amfnas@polarisnetworks.net",
                                "application/vnd.3gpp.5gnas"));
  data.append("\r\n--polarishttpmultipartboundary--");
  
  return data;
}


std::string SmContextUpdateData() {

  MsgInfo msgInfo;
  msgInfo.Initialize();
  std::string instance = "1";
  std::string AMFID = "amf.TestNetwork";
  std::string ANTYPE = "3GPP_ACCESS";
  std::string CONTENT_ID = "ngap@polarisnetworks.net";

  rapidjson::Document j_res;
  j_res.SetObject();
  rapidjson::Value amfid(AMFID.c_str(), AMFID.length());
  rapidjson::Value antype(ANTYPE.c_str(), ANTYPE.length());
  rapidjson::Value contentId(CONTENT_ID.c_str(), CONTENT_ID.length());
  rapidjson::Document n2SmInfo;
  n2SmInfo.SetObject();
  n2SmInfo.AddMember("contentId", contentId, j_res.GetAllocator());

  j_res.AddMember("amfId", amfid, j_res.GetAllocator());
  j_res.AddMember("anType", antype, j_res.GetAllocator());
  j_res.AddMember("n2SmInfo", n2SmInfo, j_res.GetAllocator());

  rapidjson::StringBuffer j_buffer_;
  rapidjson::Writer<rapidjson::StringBuffer> writer_(j_buffer_);
  j_res.Accept(writer_);
  std::string json_data = j_buffer_.GetString();
  
  std::string http_boundary = "polarishttpmultipartboundary";
  std::string data = "";
  data.append(BuildHTTPBodyPart(json_data,
                                http_boundary,
                                "json001@polarisnetworks.net",
                                "application/json"));
  
  int32_t ngap_buffer_len = NGAPMessage::kMaxBufferSize;
  uint8_t* ngap_buffer = new uint8_t[ngap_buffer_len];

  NGAPMessage::PDUSessionRscSetupRspTransfer resp_msg{};
  resp_msg.dl_ngu_addr_m.gtp_tunnel_m.teid = 2;
  resp_msg.dl_ngu_addr_m.gtp_tunnel_m.ip_address = new IPAddress("2.2.2.2", 0);


  if (true == NGAPMessage::EncodePDUSessionRscSetupRspTransfer(resp_msg, ngap_buffer,&ngap_buffer_len)) {
        data.append(BuildHTTPBodyPart(std::string(reinterpret_cast<char*>(ngap_buffer),
          ngap_buffer_len),
          http_boundary,
          "ngap@polarisnetworks.net",
          "application/vnd.3gpp.ngap"));
  } else {
    msgInfo.LOG("AMF", "Failed to encode NGAP Message: PDUSessionRscSetupRspTransfer");
  }
  data.append("\r\n--polarishttpmultipartboundary--");

  return data;
}



int main(int argc, char *argv[]) {
    boost::system::error_code ec;
    http2 server;
    server.num_threads(4);
    
    MsgInfo msgInfo;
    msgInfo.Initialize();

    HTTPClient *hc = new HTTPClient;

    hc->Connect("smf.polarisnetworks.net", "4040", sId1, &onconnect);

    server.handle("/smf/sm-contexts", [&hc](const server::request &req, const server::response & res) {
        res.write_head(200);
        int sec = 5;
        if(s1) {
        std::cout << "I will now try to ping SMF." << std::endl;

        gRes = &res; //save the response object
        
        hc->Post(sId1, "http://smf.polarisnetworks.net:4040/nsmf-pdusession/v1/sm-contexts",
                SmContextCreateData(), rId1, {{"Content-Type",
                {"multipart/related; boundary=polarishttpmultipartboundary; "
                  "type=application/json", true}}}, &onresponse);
        }
        //res.end("UDM Alive\n");
    });

    server.handle("/namf_comm/v1/ue-contexts/imsi-001019999999991/n1-n2-messages",
        [&hc](const server::request &req, const server::response &res) {
          rapidjson::Document j_res;
          j_res.SetObject();
          std::string CAUSE = "N1_N2_TRANSFER_INITIATED";
          rapidjson::Value cause(CAUSE.c_str(), CAUSE.length());
          j_res.AddMember("cause", cause, j_res.GetAllocator());
          rapidjson::StringBuffer j_buffer_;
          rapidjson::Writer<rapidjson::StringBuffer> writer_(j_buffer_);
          j_res.Accept(writer_);
          std::string data = j_buffer_.GetString();
          res.write_head(200);
          res.end(data);
        });

    server.handle("/smf/update-sm-contexts", [&hc](const server::request &req, const server::response & res) {
        res.write_head(200);
        int sec = 5;
        if(s1) {
        std::cout << "I will now try to ping SMF." << std::endl;

        gRes = &res; //save the response object
        
        hc->Post(sId1, "http://smf.polarisnetworks.net:4040/nsmf-pdusession/v1/sm-contexts/360288990189639671/modify",
                SmContextUpdateData(), rId1, {{"Content-Type",
                {"multipart/related; boundary=polarishttpmultipartboundary; "
                  "type=application/json", true}}}, &onresponse);
        }
        //res.end("UDM Alive\n");
    });

    server.handle("/ping/", [&](const server::request &req, const server::response & res) {
        res.write_head(200);
        std::cout << "/ping received: AMF alive" << "" << std::endl;
        msgInfo.LOG("AMF", "/ping received. AMF Alive");
        res.end("AMF Alive\n");
    });

    if (server.listen_and_serve(ec, "amf.polarisnetworks.net", "4040")) {
        std::cerr << "error: " << ec.message() << std::endl;
    }
}
