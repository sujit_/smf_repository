# This script removes the SMF log and restarts the Dummy UDM, UPF and PCF
cd ../../5G/log
pwd
echo "removing SMF LOG";
rm -f smf.log
cd ../config
#pwd
#echo "removing config";
#rm -f smf.json
echo "Killing UDM";
pkill -9 UDM
echo "Killing UPF";
pkill -9 UPF
echo "Killing PCF";
pkill -9 PCF
echo "";
echo "";
echo "Starting PCF";
cd ../../smf_repository/dummy_servers
./PCF &
echo "";
echo "";
echo "Starting UDM";
sleep 1
./UDM &
echo "";
echo "";
echo "Starting UPF";
sleep 1
./UPF &
echo "";
echo "";
echo "You need to start SMF and AMF now."
