// Copyright 2018 Polaris Networks (www.polarisnetworks.net).

#ifndef SERVERS_MSGINFO_HPP_
#define SERVERS_MSGINFO_HPP_

#include <fstream>
#include <string>


class MsgInfo {
 public:
  void Initialize() {
    file_.open("./dummy_servers.txt", std::ios_base::app);
    Flush();
  }
  void Finalize() { 
    file_.close();
  }
  void LOG(std::string sender, std::string msg) { 
    file_ << sender << ": " << msg << std::endl;
  }

 private:
  void Flush() { file_.flush(); }
  std::ofstream file_;
};

#endif  // SERVERS_MSGINFO_HPP_
