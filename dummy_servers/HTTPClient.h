#ifndef FRAMEWORK_HTTP_CLIENT_H_
#define FRAMEWORK_HTTP_CLIENT_H_

#include <iostream>
#include <functional>
#include <unordered_map>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <nghttp2/asio_http2_client.h>
#include <nghttp2/asio_http2_server.h>

using namespace nghttp2;
using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::server;

typedef std::function<void(const std::string, bool)> Connect_Cb;
typedef std::function<void(const std::string, const uint8_t*, std::size_t)> Response_Cb;
typedef std::function<void(const std::string, const uint8_t*, std::size_t, const server::response *)> Relay_Response_Cb;

class HTTPClient
{
public:
  HTTPClient();
  virtual ~HTTPClient();
  
  void Connect(const std::string remoteAddress, const std::string remotePort, 
    const std::string sessionId, Connect_Cb OnConnect);
  void Get(const std::string sessionId, const std::string uri, 
    const std::string requestId, Response_Cb OnResponse);
  void Relay(const std::string sessionId, const std::string uri, 
    const std::string requestId, const server::response* res, Relay_Response_Cb OnResponse);
  void Post(const std::string sessionId, const std::string uri, 
    const std::string data, const std::string requestId, const nghttp2::asio_http2::header_map header,
    Response_Cb OnResponse);
  void Disconnect(const std::string sessionId);
  
private:
  boost::asio::io_service* ioService_;
  boost::asio::io_service::work* work_;
  boost::thread_group workers_;
  std::unordered_map<std::string, asio_http2::client::session*> sessionMap_;
};

#endif //  FRAMEWORK_HTTP_CLIENT_H_
